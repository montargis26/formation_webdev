<?php
session_start();
//session_destroy() permet de supprimer la session
//mais il faut d'abord la rappeler avec session_start()
session_destroy();
?>

<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>Titre</title>
</head>
<body>
<h1> Deconnexion </h1>
<p> Vous êtes déconnecté </p>
<a href="sessions.php">Page 1</a>
    <a href="sessions2.php">Page 2</a>
    <a href="deconnexion.php">Deconnexion</a>
</body>
</html>