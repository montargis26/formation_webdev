<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <title>Page protégée</title>
</head>
<body>
    <?php
    $mdp_correct = "supermotdepassemegalong";
    
    if (isset($_POST['mdp'])){
        $mdp = $_POST['mdp'];
        if (mb_strlen($mdp) == 0){
            echo "Entrez un mot de passe";
        } elseif ($mdp == $mdp_correct){
            echo "Super secret";
        } else {
            echo "Mot de passe erroné";
        }
    }

    ?>
</body>
</html>