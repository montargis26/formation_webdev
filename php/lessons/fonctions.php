<?php
    $phrase = "Une chaîne de caractères";
    $longueur_phrase = mb_strlen($phrase);
    
    echo 'La phrase ' . $phrase . ' contient ' . $longueur_phrase . ' caractères';
    echo '</br>';
    //sprintf renvoie une chaine de caractère formatée selon un format passé en premier paramètre
    //et des variables passées en paramètres successifs 
    // voir http://php.net/manual/fr/function.sprintf.php pour plus d'informations
    echo sprintf('La phrase %2$s contient %1$d caractères, mb_strlen renvoie donc %1$d',
    $longueur_phrase, 
    $phrase);

    //remplacer une chaine de caractères par une autre 
    echo '</br>'; 
    $phrase_2 = "Tata tutu toto";
    echo str_replace('t', 'b', $phrase_2); //version sensible à la casse
    echo '</br>';
    echo str_ireplace('t', 'b', $phrase_2); //version insensible à la casse

    echo '</br>';
    //changer la casse d'une chaine 
    echo strtolower($phrase_2);
    echo '</br>';
    echo strtoupper($phrase_2);

    //utilisation de la fonction date()
    echo '</br>';
    echo date('d/m/Y H:i:s');
    echo '</br>';
    echo 'Nous sommes le ' . date('D d/m/Y') . '. Il est ' . date("H:i:s");
    echo '</br>';

    //déclaration d'une fonction
    function accueillir($nom){
        echo 'Bonjour ' . $nom . '. Nous sommes le ' . date('d/m/Y');
    }

    accueillir('Fred'); //exécution de la fonction définie plus tôt

    //rayon² * pi * hauteur * 1/3
    function volumeCone($rayon, $hauteur){
        $resultat = pow($rayon, 2) * pi() * $hauteur * (1/3);
        
        //si nos deux paramètres sont bien des nombres
        if (is_numeric($rayon) && is_numeric($hauteur)){
            return $resultat; //return interrompt l'execution de la fonction
        }
        
        //sinon la fonction continue et on peut afficher une erreur
        echo 'Erreur';
        //et renvoyer NULL
        return NULL;
    }

    $volume = volumeCone(5, 3);
    echo '</br>';
    //number format permet de spécifier le nombre de décimales d'un nombre
    echo number_format($volume, 5);

    function volumeConeAnnonce($rayon, $hauteur){
        $volume = volumeCone($rayon, $hauteur);

        //deux façons d'annoncer nos valeurs
        //à l'aide de sprintf
        echo sprintf('Le volume du cone de rayon %d et de hauteur %d est égal à %f',
        $rayon, $hauteur, $volume);
        echo '</br>';
        //à l'aide de concaténation
        echo 'Le volume du cone de rayon ' . $rayon . ' et de hauteur ' . $hauteur . ' est égal à ' . $volume;
    }
    echo '</br>';
    volumeConeAnnonce(5, 3);
?>