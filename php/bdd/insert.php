<?php

//pour ajouter des données a notre bdd
//il faut utiliser les mots clés INSERT INTO
$query_insert = "INSERT INTO appart (id, city, address, zip, surface, rooms, rent)
                VALUES ('Paris', '7 boulevard du quai', '75550', 440, 18, 12.50)";

//pour modifier une ligne de notre bdd
//il faut utiliser le mot clé UPDATE
$query_update = "UPDATE appart SET id = 1001 WHERE city = 'Paris' AND surface = 440";


//pour supprimer une ligne de notre bdd
//il faut utiliser le mot clé DELETE
$query_delete = "DELETE FROM appart WHERE id=1001";
?>