<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" type="text/css" media="screen" href="main.css">

    <title>Portail d'inscription</title>
</head>
<body>
    <form action="sign_up_process.php" method="post">
        <ul class="form_list">
            <fieldset>
                <legend>Inscription</legend>
            <li><label for="username">Pseudo</label></li>
            <li><input placeholder="j.jones" id="username" name="username" type="text" required></li>

            <li><label for="email">e-mail</label></li>
            <li><input placeholder="j.jones@nasa.gov" id="email" name="email" type="email" required></li>

            <li><label for="password">Mot de passe</label></li>
            <li><input placeholder="*******" name="password" id="password" type="password" minlength=6 required></li>

            <li><label for="password_repeat">Confirmation mot de passe</label></li>
            <li><input placeholder="*******" name="password_repeat" id="password_repeat" type="password" minlength=6 required></li>
            
            <li><button type="submit">Inscription</button> <a href="sign_in.php">Je suis déjà inscrit</a></li>
            </fieldset>
        </ul>
    </form>
    <div id="display_error">
        <?php 
        //si une erreur est renvoyée on l'affiche
            if(!empty($_GET['error_message'])){
                echo htmlspecialchars($_GET['error_message']);
            }
        ?>
</div>
</body>
</html>
