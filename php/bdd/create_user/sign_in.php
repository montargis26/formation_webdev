<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" type="text/css" media="screen" href="main.css">

    <title>Portail de connexion</title>
</head>
<body>
    <form action="sign_in_process.php" method="post">
        <ul class="form_list">
            <fieldset>
                <legend>Connexion</legend>
            <li><label for="username">Pseudo ou adresse mail</label></li>
            <li><input placeholder="j.jones" id="username" name="username" type="text" required></li>

            <li><label for="password">Mot de passe</label></li>
            <li><input placeholder="*******" name="password" id="password" type="password"></li>

            <li><button type="submit">Connexion</button><a href="sign_up.php">Je ne suis pas inscrit</a></li>
            
            </fieldset>
        </ul>
    </form>
    <div id="display_error">
        <?php 
            //si une erreur est renvoyée on l'affiche
            if (!empty($_GET['error_message'])){
                echo htmlspecialchars($_GET['error_message']);
            }
        ?>
    </div>
</body>
</html>
