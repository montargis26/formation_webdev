<?php
//déclaration de variables pour la connexion a la BDD
$host = "localhost";
$db = "test";
$user = "root";
$password = "password";

//variables pour l'exemple
$rooms = 3;
$rent_min = 300;
$rent_max = 600;


//connexion a la bdd
$connexion = new PDO('mysql:host='.$host.';dbname='.$db.';charset=utf8', $user, $password);

$query_text = "SELECT * FROM appart WHERE rent > :rent_min AND rooms = :rooms ";
$query = $connexion->prepare($query_text); //on passe notre string contenant la requête

//on demande a notre PDO de remplacer les paramètre pour nous,
//en spécifiant le type on s'assure de ne pas faire d'erreur, 
//PDO nous indiquerait si un type ne correspond pas et la requête ne fonctionnera pas
$query->bindParam(':rent_min', $rent_min , PDO::PARAM_INT); 
$query->bindParam(':rooms', $rooms,  PDO::PARAM_INT);
$query->execute();

//on affiche les données
while ($data = $query->fetch()){
    print_r($data); 
    echo '</br>';
}


?>

