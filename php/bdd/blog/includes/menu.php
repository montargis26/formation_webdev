<?php
//on vérifie qu'il y ait bien une session démarrée
if (!empty($_SESSION)) {
    //si session il y a, on vérifie que ce soit une session super utilisateur (admin)
    if ($_SESSION['super']) {
        //si c'est le cas, on affiche le menu admin
        require 'includes/menu_admin.php';
    } else {
        //sinon le menu utilisateur simple
        require 'includes/menu_user.php';
    }
} else {
    //sinon on affiche le menu des utilisateurs non connectés
    ?>

<nav>
    <ul>
    <li>
        <a href="index.php">Accueil</a>
    </li>
    <li>
        <a href="sign_in.php">Connexion</a>
    </li>
    </ul>
</nav>
<?php
}

?>