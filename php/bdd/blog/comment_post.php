<?php
//comment_post.php se charge d'ajouter le commentaire a la bdd
//selon les informations passées dans notre formulaire 

//on récupère nos fonctions rleatives a la bdd
require('includes/bdd_functions.php');
//connexion a la bdd
try {
    $pdo = bdd_connect();
} catch (PDOException $exception) {
    die($exception);
}

//tant que la verification n'est pas faite, $verification  = false
$verification = false;
//on vérifie que nos champs ne soient pas vides dans notre formulaire
if (!empty($_POST['id_article']) && !empty($_POST['author']) && !empty($_POST['content'])) {
    //on vérifie que l'id soit bien un nombre entier
    if (filter_var($_POST['id_article'], FILTER_VALIDATE_INT)){
        //si c'est le cas on indique la verification est faite en mettant $verification = true
        $verification = true;
    } else {
        //sinon on laisse $verification = false et on précise un message d'erreur
        $error_message = 'ID Article invalide';
    }
} else {
    //on laisse $verification = false et on précise un message d'erreur
    $error_message = "Information manquante, veuillez vérifier les champs du formulaire";
}

//si $verification= true (donc si les verifications sont ok)
if ($verification) {
    //on récupère l'id de l'article via POST
    $id_article = $_POST['id_article'];
    //on récupère l'auteur du commentaire via POST
    $author = $_POST['author'];
    //ainsi que le contenu
    $content = $_POST['content'];
    //puis on génère la timestamp
    $date_sent = time();
    
    //on utilise notre fonction bdd_createcomment avec tous ces paramètres
    // pour envoyer le commentaire dans notre bdd
    bdd_createComment($pdo, $id_article, $author, $content, $date_sent);
    //une fois ceci fait on renvoie vers l'article concerné
    //en précisant l'id de l'article à notre page article.php
    header('Location: article.php?id='.$id_article);
} else {
    //sinon on renvoie a la page de composition du commentaire avec un message d'erreur
    header('Location: article.php?id='.$id_article.'error_message=' . $error_message);
}
