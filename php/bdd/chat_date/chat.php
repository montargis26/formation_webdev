<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Mini chat</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" media="screen" href="main.css">
</head>
<body>
    <div class="container">
    <div class="message_container">
        <ul class="message_list">
        <?php
            $host = 'localhost';
            $db = "test";
            $user = "root";
            $password = 'password';
        
            
            try {
                $pdo = new PDO('mysql:host='.$host.';dbname='.$db.';charset=utf8', $user, $password);
                
            } catch (PDOException $exception) {
                die($exception);
            }
            
            $query = $pdo->prepare('SELECT * FROM message_date ORDER BY date_sent ASC');
            $query->execute();
            echo '<form id="effacer" method="post" action="chat_post.php">';
            while ($data = $query->fetch()){ 
                $datetime = date ('d/M/Y H:i:s' ,$data['date_sent']);
                echo '<li>';
                echo '<input type="checkbox" name="to_delete[]" value="'. $data['id'] .'">';
                echo '<span class="time_sent">'. $datetime .' </span>';
                echo '<span class="pseudo">'.$data['pseudo'].'</span>';
                echo ' : ';
                echo '<span class="m_body">'.$data['body'].'</span>';
                echo '</li>';
            }
            echo '</form>';
            $query->closeCursor();
        ?>  
        </ul>
    </div>


    <form method="post" action="chat_post.php">
        <label for="pseudo">Entrez votre Pseudo</label>
        <input required type="text" id="pseudo" name="pseudo">
        <label for="message">Tapez ici votre message :</label>
        <textarea required placeholder="Tapez ici votre message" 
        id="message" name="message" rows="5" cols="30"></textarea>
        <input type="submit" value="Envoyer">
    </form>
    </div>

    <label for="tout_effacer">Cochez les messages pour les effacer ou cochez ici pour tout effacer</label>
    <input form="effacer" type="checkbox" name="tout_effacer" id="tout_effacer"/>
    <button form="effacer" type="submit">Confirmer</button>
    </form>
    
</body>
</html>