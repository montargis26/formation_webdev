<?php

$host = 'localhost';
$db = "test";
$user = "root";
$password = 'password';

try {
    $pdo = new PDO('mysql:host='.$host.';dbname='.$db.';charset=utf8', $user, $password);
    
} catch (PDOException $exception) {
    die($exception);
}



if (isset($_POST['tout_effacer']) and $_POST['tout_effacer'] == 'on'){
    $query = $pdo->prepare('DELETE from message_date');
} else {
    if (isset($_POST['to_delete']) and count($_POST['to_delete'])!= 0){
        $to_delete = implode(',' ,$_POST['to_delete']);
        $query = $pdo->prepare('DELETE from message_date WHERE id IN ('. $to_delete .')');
    } else {

    $query = $pdo->prepare('INSERT INTO message_date (id, pseudo, body, date_sent)
    VALUES (NULL, :pseudo, :message, :date)');
    if (isset($_POST['pseudo']) && isset($_POST['message'])){
        $pseudo = $_POST['pseudo'];
        $message = $_POST['message'];
        if (trim($pseudo) != '' && trim($message) != ''){
            $query->bindValue(':pseudo', htmlspecialchars($pseudo));
            $query->bindValue(':message', htmlspecialchars($message));
            $query->bindValue(':date', time());
        } else {
            die("erreur: pseudo ou message vide. <a href='chat.php'>Retour</a>");
        }
    } else {
        die("erreur: formulaire vide. <a href='chat.php'>Retour</a>");
    }
    }
    
}
if (isset($query)){
    $query->execute();
}

header('Location: chat.php');
?>