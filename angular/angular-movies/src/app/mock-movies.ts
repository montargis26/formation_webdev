import { Movie } from './movie';

export const MOVIES: Movie[] = [
    { id: 11, title: "The Godfather", year:1972  },
    { id: 12, title: "The Shawshank Redemption" , year: 1994 },
    { id: 13, title: "Pulp Fiction", year: 1994 },
    { id: 14, title: "Star Wars" , year: 1977 },
    { id: 15, title: "GoodFellas", year: 1990 },
    { id: 16, title: "The Dark Knight" , year: 2008  },
    { id: 17, title: "The Godfather Part II" , year: 1974 },
    { id: 18, title: "The Empire Strikes Back", year: 1980 },
    { id: 19, title: "The Matrix" , year: 1999 },
    { id: 20, title: "Fight Club" , year: 1999 },
]