/*
*EXERCICE _DIFFICILE_* :

Pour ceux qui veulent aller plus loin encore et à qui les Jeux Vidéos ne font pas peur...

Vous pouvez essayer de récuperer notre classe `Personnage` et ajouter à nos personnages la gestion d'un inventaire.

Un inventaire serait matérialisé par un objet qui contiendrait différents éléments.

ex: `this.inventaire = {or: 100, arme: "épée", armure: "plaques"};`

etc...

Avec l'inventaire ajouter les méthodes adéquates (ajouter un item, jeter un item , acheter/vendre un item...) etc...
*/

class Personnage {
    constructor(nom, points_de_vie, points_de_mana, force, points_de_vie_max = 300, or = 0) {
        this.xp = 0;
        this.mort = false;
        this.nom = nom;
        this.points_de_vie_max = points_de_vie_max;
        this.points_de_vie_min = 0;
        if (points_de_vie > this.points_de_vie_max) {
            this.points_de_vie = this.points_de_vie_max;
        } else {
            this.points_de_vie = points_de_vie > 0 ? points_de_vie : 1;
        }
        this.force = force;
        this.points_de_mana = points_de_mana;

        //l'inventaire est un tableau d'objets, chaque personnage commence avec un compte d'or
        this.inventaire = [{"or": or}];
    }

    //définition d'une méthode
    decrire() {
        //cette methode fournit une description de notre personnage 
        //et liste ses caractéristiques 
        if (this.mort) {
            return `${this.nom} est décédé`;
        } else {
            return `${this.nom} a ${this.points_de_vie} points de vie restants,
            ${this.points_de_mana} de mana,
            et ${this.force} de force, 
            et ${this.xp} points d'expérience`;
        }
    }

    estMort() {
        this.mort = this.points_de_vie <= this.points_de_vie_min;
    }

    retirerPointsDeVie(n) {
        this.points_de_vie -= n;
        if (this.points_de_vie < this.point_de_vie_min) {
            this.points_de_vie = this.points_de_vie_min;
        }
        this.estMort();
    }

    ajouterPointsDeVie(n) {
        this.points_de_vie += n;
        if (this.points_de_vie > this.points_de_vie_max) {
            this.points_de_vie = this.points_de_vie_max;
        }
        this.estMort();
    }

    //préciser la valeur d'un paramètre dans la déclaration de fonction
    //permet de préciser une valeur par défaut d'un paramètre
    //cette valeur par défaut sera utilisée en cas d'absence d'argument à l'appel de la fonction
    ressuciter(n = (this.points_de_vie_max * 0.1)) {
        if (this.mort) {
            this.ajouterPointsDeVie(n);
            console.log(`Par la grâce des grands anciens, ${this.nom} retrouve la vie !`);
        } else {
            console.log(`Pas besoin de ressuciter ${this.nom}, il est toujours en vie`);
        }
    }

    attaquer(cible) {
        //si le personnage attaquant est en vie
        if (!this.mort) {
            //si la cible existe
            if (cible !== undefined) {
                //si la cible est en vie
                if (!cible.mort) {
                    //on retire un nombre de points de vie à la cible
                    //égal à la force du personnage attaquant
                    console.log(`${this.nom} attaque ${cible.nom} !`);
                    cible.retirerPointsDeVie(this.force);
                    if (cible.mort) {
                        console.log(`${cible.nom} a été tué des suites du coup porté par ${this.nom}`);
                    }
                } else {
                    console.log(`${this.nom}.attaquer : Erreur, ${cible.nom} est mort`);
                }
            } else {
                console.log(`${this.nom}.attaquer : Erreur, cible non définie`);
            }
        } else {
            console.log(`${this.nom}.attaquer : Erreur, ${this.nom} est mort`);
        }
    }

    attaquerPlusieursFois(cible, n) {
        for (let i = 0; i < n; i++) {
            this.attaquer(cible);
        }
    }
    
    //pour chercher si un item existe dans notre inventaire a partir de son nom
    //renvoie le premier item du même nom trouvé
    chercher(nom_item) {
        for (const item of this.inventaire) {
            if (String(nom_item).valueOf() === String(item.nom).valueOf()) {
                return item;
            }
        }
        console.error(`Aucun objet du nom ${nom_item} dans l'inventaire de ${this.nom}`);
    }

    //pour chercher si un item existe dans notre inventaire a partir de son nom
    //renvoie un booleen si c'est le cas
    possede(nom_item) {
        for (const item of this.inventaire) {
            if (nom_item === item.nom) {
                return true;
            }
        }
        return false;
    }
    //pour ramasser un objet item
    ramasser(item) {
        if (item instanceof Item) {
            //si on ne possède pas cet objet en particulier 

            //si on possède déjà un item du même nom
            if (this.possede(item.nom)) {
                //on récupère l'item et modifie sa quantité
                let item_existant = this.chercher(item.nom)
                item_existant.quantite += item.quantite;
                console.log(`${this.nom} ramasse ${item.nom} x${item.quantite} et range le tout avec les autres. Total possédés : ${item_existant.quantite}`);
            } else {
                //sinon on l'ajoute a notre inventaire
                console.log(`${this.nom} ramasse ${item.nom} x${item.quantite} et range le tout dans son inventaire.`)

                //on crée un nouvel objet pour que la reference de l'objet soit nouvelle à chaque fois 
                this.inventaire.push(new Item(item.nom, item.quantite, item.valeur_u));
            }

        }
    }


    //pour jeter un item, par défaut en jeter un seul
    jeter(nom_item, n = 1) {
        //si un objet de ce nom est dans l'inventaire
        if (this.possede(nom_item)){
            //on récupère l'objet
            let item_a_jeter = this.chercher(nom_item);
            //on vérifie qu'on en jette moins que la quantité possédée
            if (n < item_a_jeter.quantite){
                item_a_jeter.quantite -= n;
                console.log(`${this.nom} jette ${n} ${item_a_jeter.nom}, il en reste ${item_a_jeter.quantite} dans l'inventaire.`);
            } else {
                console.log(`${this.nom} jette ${n > 1 ? "l'entiereté de ses" : "son"} ${item_a_jeter.nom}`);
                //on supprime l'objet de l'inventaire
                this.inventaire.splice(this.inventaire.indexOf(item_a_jeter), 1);
            }
        }
    }


    //inspecte un objet de notre inventaire et renvoie sa description
    inspecter(item) {
        if (this.inventaire.includes(item)) {
            return `${this.nom} inspecte : ${item.nom}, quantité : ${item.quantite}, valeur unitaire: ${item.valeur_u}, valeur totale ${item.valeur_u * item.quantite}`;
        } else {
            return `${this.nom} ne trouve pas ${item.nom} dans son inventaire`;
        }
    }

    vendre(item, n) {
        this.inventaire[0].or = item.valeur_u * n; 
        this.jeter(item.nom, n);
    }
}


class SuperPersonnage extends Personnage {
    constructor(nom, points_de_vie, points_de_mana, force, dexterite, points_de_vie_max = 500, or = 0) {
        super(nom, points_de_vie, points_de_mana, force, points_de_vie_max, or);
        this.points_de_vie = points_de_vie;
        this.dexterite = dexterite;
    }

    decrire() {
        return super.decrire() +
            ` et ${this.dexterite} points de déxterité`;
    }
}

class Item {
    constructor(nom, quantite, valeur_u) {
        this.nom = nom;
        this.quantite = quantite;
        this.valeur_u = valeur_u;
    }
}

const jones = new Personnage("Jones", 100, 40, 20);

//création d'une épée de valeur 40 pièces d'or
const epee = new Item("épée", 1, 40);

class Epee extends Item{
    constructor(nom, quantite, valeur_u, tranchant){
        super(nom, quantite, valeur_u);
        this.tranchant = tranchant;
    }
}

let noms = ["epee", "estamaçon", "fauchon"];
let adjectifs = [" tranchante", " de force", " luisante"];
let aleanum1 = Math.floor(Math.random() * noms.length);
let aleanum2 = Math.floor(Math.random() * noms.length);
let epee2 = new Epee(noms[aleanum1] + adjectifs[aleanum2], 1, 40, 20);