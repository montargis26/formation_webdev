function ajaxGet(url, callback){
    //création d'une requête http
    let requete = new XMLHttpRequest();

    //on prépare une requête HTTP GET vers le fichier films.json
    requete.open("GET", url);
    //gestion de la requête de façon asynchrone
    requete.addEventListener("load", function(){
        //si le code de retour est bon (compris entre [200,400[)
        if (requete.status >= 200 && requete.status < 400){
            //Appel de la fonction callback en lui passant la réponse
            callback(requete.responseText);
        } else {
            //afficher l'erreur concernant notre requete
            console.error(`${requete.status} - ${requete.statusText}`);
        }
    });
    requete.addEventListener("error", function(){
        //La requête n'a pas reussi a atteindre le serveur
        console.error("La requête n'a pas atteint le serveur");
    })
    //on envoie notre requête
    requete.send();
}