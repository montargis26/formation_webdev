//GESTION DU SLIDER 
document.getElementById("filter_note").addEventListener("input", function(event){
    document.getElementById("filter_note_num").value = event.target.value;
})
//GESTION DU NUMBER INPUT
document.getElementById("filter_note_num").addEventListener("input", function(event){
    document.getElementById("filter_note").value = event.target.value;
})

//Affiche les films dans la div movie_list
//a partir d'un tableau de films
function displayMovies(tab_films){
    //on récupère la div d'id movie_list
    let div_movie_list = document.getElementById("movie_list");
    div_movie_list.innerHTML = "";

    if (tab_films.length !== 0){
        //pour chaque film dans le tableau passé en paramètre
        for (const film of tab_films){
            //on crée une div qui va contenir les infos de chaque film
            let div_film = document.createElement("div");
            div_film.id = film.id;

            //un h2 pour contenir le titre du film
            let h2_title_film = document.createElement("h2");
            h2_title_film.textContent = film.name;
            //un h4 pour contenir l'année
            let h4_year_film = document.createElement("h4");
            h4_year_film.textContent = film.year;
            //un paragrpahe pour contenir le nom du réal
            let p_dir_film = document.createElement("p");
            p_dir_film.textContent = `Réalisateur : ${film.dir_first_name} ${film.dir_last_name}`
            //un paragraphe pour contenir la note du film
            let p_note_film = document.createElement("p");
            p_note_film.textContent = `Note ${film.note}/5`

            //on ajoute tous les élèments à notre div 
            div_film.appendChild(h2_title_film);
            div_film.appendChild(h4_year_film);
            div_film.appendChild(p_dir_film);  
            div_film.appendChild(p_note_film);
            //on ajoute cette div a la div parente
            div_movie_list.appendChild(div_film);
        }
    } else {
        div_movie_list.textContent = "Aucun film correspondant à ces critères";
    }
    
    
}

//remplit les options d'un select 
//a partir d'un tableau de valeurs et du select à remplir
function fillSelect(tableau, select){
    select.innerHTML = "";
    let option_base = document.createElement("option");
    option_base.value = "N/A";
    option_base.textContent = "Choisir";
    select.appendChild(option_base);
    //pour chaque valeur du tableau passé en paramètre
    for (const valeur of tableau){
        //on crée un élèment option
        let option = document.createElement("option");
        //on affecte à l'attribut value la valeur en cours
        option.value = valeur; 
        //on affiche la valeur dans le contenu textuel de notre option
        option.textContent = valeur;
        //on ajoute notre option au select passé en paramètre
        select.appendChild(option);
    }
}

let jsonURL ="http://localhost/~ziroshell/formation/js/requetes_exercices/films.json"; 
ajaxGet(jsonURL, function(response){
    //on récupère les films
    let arr_films = JSON.parse(response);
    //on affiche les films
    displayMovies(arr_films);

    //on remplit nos tableaux d'années et de réalisateurs
    let arr_years = [];
    let arr_directors = [];
    for (const film of arr_films){
        //on empêche les doublons d'années d'entrer dans le tableau
        if (!arr_years.includes(film.year)){
            arr_years.push(film.year);
        }
        arr_directors.push(`${film.dir_first_name} ${film.dir_last_name}`)
    }
    arr_years.sort();
    arr_directors.sort();
    
    //on appelle notre fonction fillselect pour remplir nos selects
    fillSelect(arr_years, document.getElementById("filter_year_low"));
    fillSelect(arr_years, document.getElementById("filter_year_up"));
    fillSelect(arr_directors, document.getElementById("filter_director"));

    //a chaque fois que l'évènement submit est lancé
    document.getElementsByTagName("form")[0].addEventListener("submit", function(event){
        //on récupère les valeurs entrées dans notre formulaire
        let year_low = event.target.filter_year_low.value;
        let year_up = event.target.filter_year_up.value;
        let director = event.target.filter_director.value;
        let note = event.target.filter_note_num.value;
        //on applique les valeurs par défaut en cas de non remplissage
        //de certains champs
        if(year_low === "N/A"){
            //on met la borne inférieure a - l'infini pour 
            //l'année mini, comme ça elle ne sera pas prise en compte
            //dans notre comparaison
            year_low = -Infinity;
        }
        if(year_up === "N/A"){
            //même chose a l'infini pour le maxi
            year_up = Infinity;
        }
        if(!note){
            //même chose pour la note à 0
            note = 0;
        }

        //On vérifie que l'année mini ne soit pas supérieure a l'année maxi
        if (year_low > year_up){
            alert("L'année mini doit être inférieure à l'année maxi");
        } else {
            //si ce n'est pas le cas on peut filtrer en fonction des valeurs
            //du formulaire
            let arr_filtered = [];
            //pour chaque film dans notre tableau de films
            for (const film of arr_films){  
                //si les caractéristiques du film correspondent aux critères du formulaire
                //si l'année et la note correspondent
                if (film.year >= year_low && film.year <= year_up && film.note >= note){
                    //si le réalisateur est défini
                    if (director !== "N/A"){
                        //on en prend compte et on fait la comparaison
                        if (director === `${film.dir_first_name} ${film.dir_last_name}`){
                            //on ajoute le film a notre tableau filtré
                            arr_filtered.push(film);
                        }
                    } else {
                        //si le réalisateur n'est pas défini on n'en prend pas compte
                        //et on ajoute le film a notre tableau
                        arr_filtered.push(film);
                    }
                }
            }
            //on affiche nos films en appelant notre fonction displayMovies
            //mais en lui donnant notre arr_filtered au lieu de notre arr_films
            displayMovies(arr_filtered);
        }

        //preventDefault empêche à la page de se recharger au moment de l'évènement
        //submit
        event.preventDefault();
    });

    //l'évènement input sur filter_year_low nous permet de vérifier
    //quand on change l'info dans ce champ
    document.getElementById("filter_year_low").addEventListener("input", function(event){
        // on récupère l'année mini dans le formulaire
        let year_low = event.target.value;
        //on crée un tableau qui répertorie toutes les années possibles en partant
        //de l'année mini
        let sliced_array = arr_years.slice(arr_years.indexOf(parseInt(year_low)), arr_years.length-1);
        console.log(sliced_array);
        //on utilise notre fonction fillSelect pour remplir 
        //le select d'année maxi avec notre nouveau tableau
        fillSelect(sliced_array, document.getElementById("filter_year_up"));
    })

})

