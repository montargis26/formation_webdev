let el_pseudo = document.getElementById("pseudo");
el_pseudo.value = "Pseudo1";

//l'évenement focus correspond au moment ou l'utilisateur utilise un élément
el_pseudo.addEventListener("focus", function(){
    document.getElementById("aide_pseudo").textContent = "Entrez votre pseudo, uniquement à l'aide de lettres et chiffres";
});

//l'évènement blur correspond au moment ou l'utilisateur passe d'un élément à un autre
//l'element délaissé est affecté par l'évènement blur
el_pseudo.addEventListener("blur", function(){
    document.getElementById("aide_pseudo").textContent = "";
});

//forcer le focus sur un element, ici l'élément pseudo
el_pseudo.focus();

//les checkbox envoient l'évènement change lorsque leur état change
//leur état (cochée ou non cochée) 
document.getElementById("confirmation").addEventListener("change", function(event){
    console.log(`Demande confirmation ${event.target.checked}`);
});

//les bouton radio répondent au même évènement change lorsqu'on les coche/décoche
let el_abos = document.getElementsByName("abonnement");
for (const el_abo of el_abos){
    el_abo.addEventListener("change", function(event){
        console.log(`Abonnement choisi ${event.target.value}`)
    });
}

//les listes déroulantes (select) répondent au même évènement change lorsque 
//l'on selectionne une valeur de la liste
document.getElementById("nationalite").addEventListener("change", function(event){
    //la value du select correspond a la value de l'option selectionnée
    console.log(`Nationalité ${event.target.value}`);
});

//récupération de notre element form
let formulaire = document.getElementsByTagName("form")[0];
//tous les elements propre au formulaire sont contenus dans la propriété
//elements, qui est une collection d'elements
console.log(formulaire.elements);
//le nombre d'elements propres au formulaire est récupéré via length
console.log(formulaire.elements.length)
//un element particulier de notre formulaire peut se récuperer via son nom
//qui est devenu une propriété de elements
//ici on récupère l'element input de nom mdp
console.log(formulaire.elements.mdp);
//ici on récupère la value de l'element de nom pseudo
console.log(formulaire.elements.pseudo.value);

formulaire.addEventListener("submit", function(event){
    //récupérer les valeurs des champs texte
    let pseudo = event.target.pseudo.value;
    let mdp = event.target.mdp.value;
    let mail = event.target.mail.value;
    //on les affiche dans la console
    console.log(`Le pseudo choisi est ${pseudo}, 
    votre mot de passe ${mdp}, 
    et votre mail ${mail}`)
    
    //on affiche un message différent si la case confirmation est cochée ou non
    if (event.target.confirmation.checked){
        console.log("Confirmation d'inscription demandée");
    } else {
        console.log("Confirmation d'inscription refusée");
    }

    //pour verifier les valeurs de boutons radio, switch est tout indiqué
    switch (event.target.abonnement.value) {
        case "abocomplet":
            console.log("Abonnement à la newsletter et aux promos");
            break;
        case "abonews":
            console.log("Abonnement à la newsletter");
            break;
        case "abonon":
            console.log("Abonnement refusé");
            break;
        default: 
            console.error("Code d'abonnement non reconnu");
    }
    //de même pour les listes déroulantes avec select
    switch (event.target.nationalite.value) {
        case "FR":
            console.log("Nationalité française");
            break;
        case "BE":
            console.log("Nationalité belge");
            break;
        case "CH":
            console.log("Nationalité suisse");
            break;
        default: 
            console.error("Code de nationalité non reconnu");
    }


    event.preventDefault();
});

//l'evenement input intervient lorsqu'on modifie la valeur d'un champ input
formulaire.mdp.addEventListener("input", function(event){
    //ce code permet de changer l'aide a l'utilisateur en fonction de la longueur du mdp
    let mdp = event.target.value;
    let liste_force_mdp = ["faible", "moyenne", "bonne", "élevée"];
    let liste_couleur_mdp = ["red", "orange", "yellow", "green"];
    let force_mdp = 0;
    if (mdp.length >= 4){
        force_mdp = 1;
    }
    if (mdp.length >= 8){
        force_mdp = 2;
        let el_pseudo = document.getElementById("pseudo");
        el_pseudo.value = "Pseudo1";
        
        //l'évenement focus correspond au moment ou l'utilisateur utilise un élément
        el_pseudo.addEventListener("focus", function(){
            document.getElementById("aide_pseudo").textContent = "Entrez votre pseudo, uniquement à l'aide de lettres et chiffres";
        });
        
        //l'évènement blur correspond au moment ou l'utilisateur passe d'un élément à un autre
        //l'element délaissé est affecté par l'évènement blur
        el_pseudo.addEventListener("blur", function(){
            document.getElementById("aide_pseudo").textContent = "";
        });
        
        //forcer le focus sur un element, ici l'élément pseudo
        el_pseudo.focus();
        
        //les checkbox envoient l'évènement change lorsque leur état change
        //leur état (cochée ou non cochée) 
        document.getElementById("confirmation").addEventListener("change", function(event){
            console.log(`Demande confirmation ${event.target.checked}`);
        });
        
        //les bouton radio répondent au même évènement change lorsqu'on les coche/décoche
        let el_abos = document.getElementsByName("abonnement");
        for (const el_abo of el_abos){
            el_abo.addEventListener("change", function(event){
                console.log(`Abonnement choisi ${event.target.value}`)
            });
        }
        
        //les listes déroulantes (select) répondent au même évènement change lorsque 
        //l'on selectionne une valeur de la liste
        document.getElementById("nationalite").addEventListener("change", function(event){
            //la value du select correspond a la value de l'option selectionnée
            console.log(`Nationalité ${event.target.value}`);
        });
        
        //récupération de notre element form
        let formulaire = document.getElementsByTagName("form")[0];
        //tous les elements propre au formulaire sont contenus dans la propriété
        //elements, qui est une collection d'elements
        console.log(formulaire.elements);
        //le nombre d'elements propres au formulaire est récupéré via length
        console.log(formulaire.elements.length)
        //un element particulier de notre formulaire peut se récuperer via son nom
        //qui est devenu une propriété de elements
        //ici on récupère l'element input de nom mdp
        console.log(formulaire.elements.mdp);
        //ici on récupère la value de l'element de nom pseudo
        console.log(formulaire.elements.pseudo.value);
        
        formulaire.addEventListener("submit", function(event){
            //récupérer les valeurs des champs texte
            let pseudo = event.target.pseudo.value;
            let mdp = event.target.mdp.value;
            let mail = event.target.mail.value;
            //on les affiche dans la console
            console.log(`Le pseudo choisi est ${pseudo}, 
            votre mot de passe ${mdp}, 
            et votre mail ${mail}`)
            
            //on affiche un message différent si la case confirmation est cochée ou non
            if (event.target.confirmation.checked){
                console.log("Confirmation d'inscription demandée");
            } else {
                console.log("Confirmation d'inscription refusée");
            }
        
            //pour verifier les valeurs de boutons radio, switch est tout indiqué
            switch (event.target.abonnement.value) {
                case "abocomplet":
                    console.log("Abonnement à la newsletter et aux promos");
                    break;
                case "abonews":
                    console.log("Abonnement à la newsletter");
                    break;
                case "abonon":
                    console.log("Abonnement refusé");
                    break;
                default: 
                    console.error("Code d'abonnement non reconnu");
            }
            //de même pour les listes déroulantes avec select
            switch (event.target.nationalite.value) {
                case "FR":
                    console.log("Nationalité française");
                    break;
                case "BE":
                    console.log("Nationalité belge");
                    break;
                case "CH":
                    console.log("Nationalité suisse");
                    break;
                default: 
                    console.error("Code de nationalité non reconnu");
            }
        
        
            event.preventDefault();
        });
        
        //l'evenement input intervient lorsqu'on modifie la valeur d'un champ input
        formulaire.mdp.addEventListener("input", function(event){
            //ce code permet de changer l'aide a l'utilisateur en fonction de la longueur du mdp
            let mdp = event.target.value;
            let liste_force_mdp = ["faible", "moyenne", "bonne", "élevée"];
            let liste_couleur_mdp = ["red", "orange", "yellow", "green"];
            let force_mdp = 0;
            if (mdp.length >= 4){
                force_mdp = 1;
            }
            if (mdp.length >= 8){
                force_mdp = 2;
            }
            if (mdp.length >= 11){
                force_mdp = 3;
            }
            let aide_mdp = document.getElementById("aide_mdp");
            aide_mdp.textContent = `Sécurité du mot de passe : ${liste_force_mdp[force_mdp]}`;
            aide_mdp.style.color = liste_couleur_mdp[force_mdp];
        });
        
            }
    if (mdp.length >= 11){
        force_mdp = 3;
    }
    let aide_mdp = document.getElementById("aide_mdp");
    aide_mdp.textContent = `Sécurité du mot de passe : ${liste_force_mdp[force_mdp]}`;
    aide_mdp.style.color = liste_couleur_mdp[force_mdp];
});

