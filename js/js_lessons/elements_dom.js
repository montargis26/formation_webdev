//DOM = Document Object Model
//document définit l'entièreté de la page
//document est l'objet qui représente la balise html dans le DOM
let page = document;

//comme document est un objet, on peut acceder a ses propriétés
let head = document.head;
let body = document.body;
console.log(head, body);

//chaque noeud de notre document est également un objet
//ces objets contiennent des propriétés ainsi que des méthodes
if (document.body.nodeType === document.ELEMENT_NODE) {
    console.log("Body est un noeud element");
} else {
    console.log("Body est un noeud textuel");
}

//la propriété childNodes contient par exemple un tableau d'objet
//ces objets sont les noeuds enfants de l'objet qui appelle childNodes
console.log(body.childNodes);

if (body.childNodes[0].nodeType === document.ELEMENT_NODE){
    console.log("Le premier enfant de body est un noeud element");
} else {
    console.log("Le premier element de body est un noeud textuel");
}

//parcours la liste des noeuds enfants de body 
//et affiche s'ils sont element ou textuels 
for (let i = 0; i < body.childNodes.length; i++){
    console.log(body.childNodes[i]);
    if (body.childNodes[i].nodeType === document.ELEMENT_NODE){
        console.log(`enfant ${i} de body est un noeud element`);
    } else {
        console.log(`enfant ${i} de body est un noeud textuel`);
    }
}

//récuperer tous les noeuds h2
let titres_h2 = document.getElementsByTagName("h2");

console.log(titres_h2);
//affiche le nombre d'elements h2 trouvés
console.log(titres_h2.length);
//affiche le premier element h2 trouvé
console.log(titres_h2[0]);

//récupère tous les noeuds avec la classe emphase
let elements_emphase = document.getElementsByClassName("emphase");
console.log(elements_emphase);

//récupère UN element via son identifiant
let liste1 = document.getElementById("liste1");
console.log(liste1);

//récupère plusieurs éléments via un sélecteur CSS
//récupère les éléments de classe emphase contenus dans
//un element d'id liste1
let liste1_emphase = document.querySelectorAll("#liste1 .emphase");
//récupère les éléments de classe emphase contenus dans
//un element d'id liste2
let liste2_emphase = document.querySelectorAll("#liste2 .emphase");
console.log(liste1_emphase, liste2_emphase);

//renvoie le premier élément répondant au selecteur CSS
let liste1_premier_elem = document.querySelector("#liste1 .emphase");
console.log(liste1_premier_elem)

//recuperer l'html contenu dans un element
let main= document.getElementsByTagName("main")[0];
console.log(main.innerHTML);

//recuperer le contenu textuel d'un element
console.log(main.textContent);

//recuperer la valeur d'un attribut
let lien_site = document.getElementsByTagName("a")[0];
console.log(lien_site.getAttribute("href"));
console.log(lien_site.href)

console.log(liste1.getAttribute("id"));
console.log(liste1.id);

//verifie si un element possède un attribut particulier
//renvoie un booléen
console.log(lien_site.hasAttribute("target"));

let p_site2 = document.getElementById("p_site2");
//recupere la liste des classes d'un element
//renvoie un tableau 
let p_site2_classes = p_site2.classList;

console.log(p_site2_classes.length);
console.log(p_site2_classes[0]);

//verifier si une classe est présente dans une liste de classes
//renvoie un booléen
console.log(p_site2_classes.contains("grand"));



