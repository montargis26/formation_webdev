
//EXERCICE
//Afficher la moyenne des valeurs d'un tableau
// [1,4,8,19,5,20,3,9,0,-1,-6,30,11,50,4,9]

// affiche la moyenne des nombres d'un tableau tableau 
// renvoie un nombre 
function calculer_moyenne(tableau) {
    let somme = 0;
    for (let i = 0; i < tableau.length; i++) {
        somme += tableau[i];
    }
    return somme / tableau.length;
}

let tab_notes = [1, 4, 8, 19, 5, 20, 3, 9, 0, -1, -6, 30, 11, 50, 4, 9];
console.log(calculer_moyenne(tab_notes));


//EXERCICE
//Convertir un tableau de températures de Fahrenheit à Celsius
// (F - 32) x 5/9 = C
let temperatures_f = [32, 41, 48.2, 68, 55, 75.5, 43, 16, 64.3, 87];

function conv_f_vers_c(tableau) {
    let celsius = [];
    for (let i = 0; i < tableau.length; i++) {
        celsius.push((tableau[i] - 32) * (5 / 9));
    }
    return celsius;
}
let temperatures_c = conv_f_vers_c(temperatures_f);
console.log(temperatures_f);
console.log(temperatures_c);

//EXERCICE
//Compter le nombre de pairs dans un tableau
//ainsi que le nombre d'impairs
//ex tableau [1,4,8,19,5,20,3,9,0,-1,-6,30,11,50,4,9]

//compte le nombre de pairs et impairs 
//dans un tableau de valeurs numériques tableau 
//renvoie un tableau de deux valeurs 
//la première valeur étant le nombre de pairs
function compter_pairs_impairs(tableau) {
    let nb_pairs = 0;
    let nb_impairs = 0;
    for (let i = 0; i < tableau.length; i++) {
        if (tableau[i] % 2 === 0) {
            nb_pairs++;
        } else {
            nb_impairs++;
        }
    }
    return [nb_pairs, nb_impairs];
}

let tab_nombres = [1, 4, 8, 19, 5, 20, 3, 9, 0, -1, -6, 30, 11, 50, 4, 9, 48];
console.log(tab_nombres);
console.log(compter_pairs_impairs(tab_nombres));

//EXERCICE 
//Récuperer les valeurs d'un tableau tableau 
//dans un intervalle défini par les bornes a et b

function recup_intervalle(tableau, a, b) {
    let resultat = [];
    tableau.forEach((num) => {
        if (num >= a && num <= b) {
            resultat.push(num);
        }
    });
    /*
    for (let i = 0; i < tableau.length; i++){
        if (tableau[i] >= a && tableau[i] <= b){
            resultat.push(tableau[i]);
        }
    }
    */
    return resultat;
}

let prix = Array.from({ length: 400 }, () => Math.floor(Math.random() * 500));

console.log(recup_intervalle(prix, 100, 200));

//EXERCICE
//Vérifier qu'une valeur n existe
//dans un tableau de valeurs tableau
//renvoie true ou false
function valeur_incluse(tableau, n) {

    for (let i = 0; i < tableau.length; i++){
        if (n === tableau[i]){
            return true;
        }
    }
    return false;
}
let tab = [100,240,22,454,52,230,455,357,414,223,24,207,70,233,131,342,331,2,396,250];

console.log(valeur_incluse(tab, 22)); //renvoie true
console.log(valeur_incluse(tab, -3)); //renvoie false

//EXERCICE
//fizz buzz
//Ecrire une fonction qui affiche les nombres de 1 à 100 
//et affiche dans la console fizz à la place du nombre si le nombre est divisible par 3
//affiche buzz à la place du nombre si le nombre est divisible par 5
//et affiche fizzbuzz à la place du nombre si le nombre est divisible par 3 et par 5

function fizzBuzz(){
    for (let i = 1; i <= 100; i++){
        if(i%3 === 0 && i%5 === 0){
            console.log("fizzbuzz");
        } else if(i%5 === 0) {
            console.log("buzz")
        } else if(i%3 === 0){
            console.log("fizz")
        } else {
            console.log(i);
        }
    }
}

fizzBuzz();


//EXERCICE
//Ranger dans un autre tableau les nombres premiers
//contenus dans un tableau tab

//METHODE "SIMPLE"
function rangerPrimes(tab){
    let premiers = []
    let estPremier = true;
    for(let i = 0; i<tab.length; i++){
        estPremier = (tab[i] !== 1 && tab[i] !== 0);
        for(let j = 2; j < tab[i]; j++){
            if(tab[i] % j === 0){
                estPremier = false;
                break;
            }
        }
        if (estPremier){
            premiers.push(tab[i]);
        }
    }
    return premiers;
}

// AUTRE METHODE "COMPLEXE"

function estPremier(n) {
    for(let i = 2; i < n; i++){
        //si notre nombe a un diviseur autre que lui même
        if(n % i === 0){
            return false; 
        }      
    }
    return (n !== 1 && n !== 0); //return true ou false
}

function rangerPremiers(tab){
    let premiers = [];
    for(let i = 0; i<tab.length; i++){
        if (estPremier(tab[i])){
            premiers.push(tab[i]);
        }
    }
    return premiers;
}

let nombres_vrac = [24,11,7, 1,34,21,13,27,34,42,26,30,34,48,42,22,28,1,12,40,22,5,30,31,29,11];
console.log(rangerPremiers(nombres_vrac));
console.log(rangerPrimes(nombres_vrac));


//EXERCICE DIFFICILE
//Implémenter le https://fr.wikipedia.org/wiki/Crible_d%27%C3%89ratosth%C3%A8ne
//crible d'eratosthene 
//ecrire une fonction permettant de générer les 100 premiers nombres premiers
//à l'aide du crible d'eratosthene 

//EXERCICE "FACILE"
//Multiplier les nombres d'un tableau 
//par les nombres correspondants dans un autre tableau de même taille
//[x,y,z]
//[a,b,c]
//renvoyer un tableau contenant le résultat 
//[a*x, b*y, c*z] 
//en cas d'erreur renvoie une chaine de caractères

function multiplierTableaux(tab1, tab2){
    if (tab1.length !== tab2.length){
        return "erreur, longueur des tableaux inégales";
    }

    let tab_resultat = [];
    for (let i = 0; i < tab1.length; i++){
        tab_resultat.push(tab1[i] * tab2[i]);
    }
    return tab_resultat;
}
let tablo = [2,4,6];
let tablo2 = [1,2,3]
console.log(multiplierTableaux(tablo, tablo2));