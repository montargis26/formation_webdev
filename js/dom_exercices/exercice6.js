let articles = [
    {
        titre: "Article 1",
        texte: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque egestas dapibus odio rutrum tincidunt. Curabitur tincidunt porttitor sem eget mattis. Aenean rhoncus lorem tellus, nec fringilla urna porta eu. Cras porta nisl eu tellus facilisis, nec faucibus justo egestas. Aliquam erat volutpat. Sed nec euismod libero. Duis et massa a odio dignissim faucibus eget eget augue. Cras malesuada risus sapien, ac feugiat dui pulvinar vitae. Ut quis mi viverra, viverra est ut, imperdiet nulla.",
        img_url: "https://via.placeholder.com/300x150"
    },
    {
        titre: "Article 2",
        texte: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent non lobortis justo. Pellentesque commodo orci felis, vitae rhoncus odio mattis sed. Curabitur varius dapibus metus, sed eleifend ipsum maximus vitae. Sed molestie, diam in luctus porttitor, turpis dolor cursus quam, ut semper metus mauris gravida risus. Fusce id nulla quis lorem volutpat dapibus. Suspendisse blandit enim sit amet purus eleifend mollis. Sed feugiat nisl et condimentum vulputate. Suspendisse mattis enim at dignissim auctor. Praesent ullamcorper mollis placerat. Nam ipsum nunc, suscipit nec massa quis, lobortis fringilla leo. Mauris ac justo nec quam porttitor laoreet. Nulla porttitor enim leo, in blandit elit pulvinar ac. Duis et metus laoreet, mollis elit suscipit, consectetur est. Nam massa ipsum, pharetra nec massa id, ultrices faucibus velit.",
        img_url: "https://via.placeholder.com/320x150"
    },
    {
        titre: "Article 3",
        texte: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi feugiat nisi ut felis molestie, a tempus purus lobortis. Vestibulum mauris risus, placerat at purus et, commodo pulvinar mi. Integer facilisis ex lacus, non malesuada purus congue a. Sed id ante sed nibh venenatis mattis. Mauris egestas nisl lacus, vel euismod risus interdum sed. Fusce posuere malesuada elit quis luctus. Maecenas eu luctus lectus. Nunc elementum eu enim eget varius. Curabitur mollis neque vel imperdiet pellentesque. Sed egestas quis nulla sed pulvinar. In ut venenatis felis. Nulla ultrices pellentesque velit, ut tempus urna maximus vel. Duis eget pretium urna.",
        img_url: "https://via.placeholder.com/200x350"
    },
    {
        titre: "Article 4",
        texte: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque et ligula at eros maximus vulputate at at ipsum. Nullam bibendum pellentesque metus, sit amet eleifend ante commodo eu. Quisque pulvinar a augue ac malesuada. Aliquam quis ex nec elit semper tincidunt. Aenean congue eros in sapien molestie, quis rutrum tortor laoreet. Nam non posuere dui. Pellentesque ultrices commodo mi, ac cursus mi lacinia sit amet. Pellentesque blandit pulvinar magna, a feugiat nibh. Donec varius tellus commodo elit dictum luctus. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Praesent imperdiet sem vitae tincidunt gravida. Morbi quis nisi at nisl pellentesque feugiat sed nec eros. Ut tristique, erat non facilisis tincidunt, neque odio finibus lectus, in ultrices lectus lorem id diam. Nullam libero sem, dapibus et risus et, sollicitudin aliquam purus. Curabitur purus magna, tincidunt at justo ac, condimentum pulvinar metus. Sed ac sapien sollicitudin, gravida libero finibus, finibus tortor. ",
        img_url: "https://via.placeholder.com/450x150"
    }
]

//pour chaque objet du tableau
//afficher le titre de chaque objet dans un h1
//le texte de chaque objet dans un p
//l'img_url de chaque objet servira a afficher une image 
//chaque article sera séparé d'une balise hr

let contenu = document.getElementById("contenu");

for (const article of articles){
    let titre = document.createElement("h1");
    let paragraphe = document.createElement("p");
    let img = document.createElement("img");
    let hr = document.createElement("hr");

    titre.textContent = article.titre;
    paragraphe.textContent = article.texte;
    img.src = article.img_url;

    contenu.appendChild(titre);
    contenu.appendChild(paragraphe);
    contenu.appendChild(img);
    contenu.appendChild(hr);
}