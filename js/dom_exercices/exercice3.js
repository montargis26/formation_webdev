let urls = ["https://www.google.fr",
            "https://developer.mozilla.org/fr/",
            "https://fr.wikipedia.org/wiki/",
            "htpps://www.amazon.fr"
            ]

//ajouter des liens dont l'attribut href est 
//pour chacun une url contenue dans le tableau

let el_contenu = document.getElementById("contenu");

function afficherLiens(tableau_urls){
    for (const str_url of tableau_urls){
        //on crée un élément de type anchor
        let el_ancre = document.createElement("a");
        //on modifie le contenu textuel de cet element
        el_ancre.textContent = str_url;
        //on affecte l'attribut href de cet element
        el_ancre.href = str_url;
        //on ajoute l'element en temps qu'enfant de contenu
        el_contenu.appendChild(el_ancre);
        //on ajoute un bloc saut de ligne juste après
        el_contenu.appendChild(document.createElement("br"));
    }
}

afficherLiens(urls);
